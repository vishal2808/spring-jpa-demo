# README #

### What is this repository for? ###

* This maven web app demonstrates how to configure Spring Framework to communicate with database using JPA and Hibernate as the JPA vendor.
The benefits of using Spring Data is that it removes a lot of boiler-plate code and provides a cleaner and more readable implementation of DAO layer. Also, it helps make the code loosely coupled and as such switching between different JPA vendors is a matter of configuration.
* 1.0.1