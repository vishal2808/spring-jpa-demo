package com.java.spring.dao;

import com.java.spring.entity.Employee;

public interface EmployeeDAO extends BaseDao<Employee> {

}
