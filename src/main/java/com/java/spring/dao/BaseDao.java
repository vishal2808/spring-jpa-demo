package com.java.spring.dao;

import java.sql.SQLException;
import com.java.spring.entity.BaseEntity;

public interface BaseDao<T extends BaseEntity> {

    public void save(T entity) throws SQLException;

    public T findById(Long id) throws SQLException;

}
