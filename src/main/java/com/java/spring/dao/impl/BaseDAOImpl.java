package com.java.spring.dao.impl;

import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.java.spring.entity.BaseEntity;
import com.java.spring.entity.Employee;

public class BaseDAOImpl<T extends BaseEntity> {

    @PersistenceContext
    protected EntityManager entityManager;
    Class<T> typeParameterClass;

    public Class<T> getTypeParameterClass() {
        return typeParameterClass;
    }

    public void setTypeParameterClass(Class<T> typeParameterClass) {
        this.typeParameterClass = typeParameterClass;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void save(T entity) throws SQLException {
        entityManager.persist(entity);
    }

    public T findById(Long id) throws SQLException {
        return entityManager.find(typeParameterClass, id);
    }

}
