package com.java.spring.dao.impl;

import java.sql.SQLException;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.java.spring.dao.EmployeeDAO;
import com.java.spring.entity.Employee;

@Repository("EmployeeDAOImpl")
@Transactional(propagation = Propagation.REQUIRED)
public class EmployeeDAOImpl extends BaseDAOImpl<Employee> implements EmployeeDAO {

    public EmployeeDAOImpl() {
        super.setTypeParameterClass(Employee.class);
    }

    public void save(Employee employee) throws SQLException {
        super.save(employee);
    }

    public Employee findById(Long employeeId) throws SQLException {
        return (Employee) super.findById(employeeId);
    }

}
