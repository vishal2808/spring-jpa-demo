package com.java.spring;

import java.sql.SQLException;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.java.spring.dao.EmployeeDAO;
import com.java.spring.entity.Employee;

public class SpringDataDemo {

    public static void main(String[] args) {
        try {
            ApplicationContext context = new ClassPathXmlApplicationContext("spring-configuration.xml");

            //Fetch the DAO from Spring Bean Factory
            EmployeeDAO employeeDAO = (EmployeeDAO) context.getBean("EmployeeDAOImpl");
            Employee employee = new Employee("Employee123");
			//employee.setEmployeeId("1");

            //Save an employee Object using the configured Data source
            employeeDAO.save(employee);
            System.out.println("Employee Saved with EmployeeId " + employee.getId());

            //find an object using Primary Key
            Employee emp = employeeDAO.findById(employee.getId());
            System.out.println(emp);

            //Close the ApplicationContext
            ((ConfigurableApplicationContext) context).close();
        } catch (BeansException | SQLException e) {
            e.printStackTrace();
        }
    }
}
